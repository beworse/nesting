# NESTING

Celem jest stworzenie oprogramowania umożliwiającego układanie dowolnych n wymiarowych figur w dwóch wymiarach, aby jak najefektywniej wykorzystać dana przestrzeń.

### Biblioteki

PyQt4, OpenGL, Vectors

### RESEARCH
Czyli dokumenty umożliwiające wdrożenie w temat
[pomocne dokumenty](https://drive.google.com/drive/folders/0BxS-H0L5mYbqZG1NQ3VxVFpQQzg?usp=sharing)

### PRZYKŁADOWY PLIK .json

W celu ułatwienia testów został stworzony prosty plik, dzięki któremu w łatwy sposób możliwe jest odtworzenie danej sytuacji. W poniższym przkładdzie umieszczone zostały dwie figury (trójkąty) o współrzednych x,y:

```
{
    "figures": [
    
        [
        
            [
            
                20, 
                
                20
                
            ], 
            
            [
            
                40,
                
                20
                
            ],
            
            [
            
                20,
                
                40
                
            ]
            
        ],
        
        [
        
            [
            
                20, 
                
                20
                
            ], 
            
            [
            
                40,
                
                20
                
            ],
            
            [
            
                20,
                
                40
                
            ]
            
        ]
        
    ]
    
}
```

#TODO

###Główne:

- Poprawienie wydajności

- Wczytywanie dxf

- Wymyslć sposób sprawdzenia czy figura jest zamknięta i jest wczytywana poprawnie (CCW)

- Otwory w figurach (modyfikacja ear clipping)

###Dodatkowe:

- Wczytywanie domyślnej konfiguracji z pliku, o ile istnieje
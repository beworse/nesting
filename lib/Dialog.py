#!/usr/bin/python
# -*- coding: utf-8 -*-
import PyQt4
from PyQt4 import QtCore

from PyQt4.QtGui import QWidget 
from PyQt4.QtGui import QVBoxLayout, QHBoxLayout #layouty
from PyQt4.QtGui import QLabel, QLineEdit, QPushButton, QCheckBox #gui
from PyQt4.QtCore import Qt

# ---- FUNKCJE WSPOLNE----#
''' Tworzy przyciski Zastosuj i Anuluj
Argumenty:
mLyout(QVBoxLayout/QHBoxLayout): layout do ktorego ma zostac dodana utworzony obiekt 

Zwraca:
applyButton(QPushButton): przycisk zastosuj
cancelButton(QPushButton): przycisk anuluj
'''
def create2Buttons(mLayout):
    layout = QHBoxLayout()
    mLayout.addLayout(layout)
    
    applyButton = QPushButton("Zastosuj") 
    layout.addWidget(applyButton)
    
    cancelButton = QPushButton("Anuluj")
    layout.addWidget(cancelButton)
    
    return applyButton, cancelButton
    
''' Tworzy przyciski Ok, Zastosuj i Anuluj
Argumenty:
mLyout(QVBoxLayout/QHBoxLayout): layout do ktorego ma zostac dodana utworzony obiekt 

Zwraca:
okButton(QPushButton): przycisk ok
applyButton(QPushButton): przycisk zastosuj
cancelButton(QPushButton): przycisk anuluj
'''
def create3Buttons(mLayout):
    layout = QHBoxLayout()
    mLayout.addLayout(layout)
    
    okButton = QPushButton("Ok")
    layout.addWidget(okButton)
    
    applyButton = QPushButton("Zastosuj") 
    layout.addWidget(applyButton)
    
    cancelButton = QPushButton("Anuluj")
    layout.addWidget(cancelButton)
    
    return okButton, applyButton, cancelButton
    
''' Dodaje label z opisem sekcji
Argumenty:
sectionName(str): nazwa sekcji
mLyout(QVBoxLayout/QHBoxLayout): layout do ktorego ma zostac dodana utworzony obiekt 
'''
def createSection(sectionName, mLayout):
    label = QLabel(sectionName)
    label.setStyleSheet("font-weight: bold;");
    label. setAlignment(Qt.AlignHCenter) 
    mLayout.addWidget(label)

''' Opis i pole do edycji liczby zmiennoprzecinkowej
Argumenty:
labelText(str): opis ustawianej wartoscim 
mLyout(QVBoxLayout/QHBoxLayout): layout do ktorego ma zostac dodana utworzony obiekt 

Zwraca:
lineEdit(QLineEdit): edytowalna wartosc
'''
def createLabelValue(labelText,mLayout):
#utworzenie layoutu 
    layout = QHBoxLayout()
    layout.setMargin(0)
    
#utworzenie opisu wartosci 
    label = QLabel(labelText)
    layout.addWidget(label)
    
#edytowalna wartosc
    lineEdit = QLineEdit()
    layout.addWidget(lineEdit)
    
#dodanie do glownego layout
    mLayout.addLayout(layout)
    
    return lineEdit
    
''' Opis i checkbox do edycji zmiennej typu True/False
Argumenty:
labelText(str): opis ustawianej wartoscim 
mLyout(QVBoxLayout/QHBoxLayout): layout do ktorego ma zostac dodana utworzony obiekt 

Zwraca:
checkBox(QCheckBox): edytowalna wartosc jako checked
'''
def createCheckValue(labelText, mLayout):
#utworzenie layoutu
    layout = QHBoxLayout()
    layout.setMargin(0)
    
#utworzenie opisu wartosci
    label = QLabel(labelText)
    layout.addWidget(label)
    
#edytowalna wartosc
    checkBox = QCheckBox()
    clayout = QHBoxLayout()
    clayout.addWidget(checkBox)
    clayout.setAlignment(Qt.AlignRight)  #checkbox nie ma setAlignment
    layout.addLayout(clayout)
    
#dodanie do glownego layout
    mLayout.addLayout(layout)
    
    return checkBox

# ---- KLASA ----#
'''Dynamicznie tworzony dialog '''
class DynamicDialog(QWidget):
# ---- SYGNAL(Y) ----#
    s_ValueChanged = QtCore.pyqtSignal() #sygnal emitowany po wcisnieciu przycisku zastosuj
    
# ---- INICJALIZACJA KLASY ----#
    '''Tworzy wyglad interfejsu uzytkownika
    Argumenty:
        strArray(lista): lista argumentow niezbedna do tworzenia ui (patrz createUi)
        WindowTitle(str): nazwa okna
    '''
    def __init__(self,strArray, WindowTitle = ""):
        QWidget.__init__(self)
        self.variables()
        self.createUi(strArray)
        self.setWindowTitle(WindowTitle) #nazwa okna
      
    '''Tworzy zmienne'''
    def variables(self):
        self.dicSet = {} 
        self.dicRead = {}
        
    '''Tworzy gui
    Argumenty:
        strArray[ListaElement]: lista zawierajaca liste ListaElement. 
            Przykladowy Element z ListaElement
            - ["none","Nazwa"]: uzywa funkcji createSection("Nazwa",mLayout)
            - ["label","labelText","nazwaZmiennej",WARTOSC]: uzywa funkcji createLabelValue(labelText,mLayout) a potem ustawia wyswetlana wartosc na WARTOSC. WARTOSC to float
            - ["checkbox","labelText","nazwaZmiennej",WARTOSC]: uzywa funkcji createCheckValue(labelText, mLayout), a potem ustawia wartosc na WARTOSC. WARTOSC to bool     
            mlayout to glowny layout ktory nie powinien nas iteresowac jest on utworzony automatycznie.
            
            Przyklad strArray:
                    DynamicDialog([\
                                    ["none","Przykladowe parametry:"],\
                                    ["label","Uwaga bedzie float","valueFloat",1.0],\
                                    ["checkbox","Printowac debuga? Uwaga typ bool","printBool",True],\
                            ],\
                            "Nazwa okna"
                            )
    '''
    def createUi(self, strArray):
    #glowny layout
        self.mainLayout = QVBoxLayout()
        self.mainLayout.setMargin(0)
        self.setLayout(self.mainLayout)
        
    #tworzenie gui na podstawie strArray
        for element in strArray:
            if type(element) is type([]):
                if(len(element) == 2):
                    myType = element[0]
                    sectionName = element[1]
                    
                    if myType == "none":
                        createSection(sectionName,self.mainLayout)
                    
                elif(len(element) == 4):
                    myType = element[0]
                    myLabelName = element[1]
                    myValueName = element[2]
                    myValue = element[3]
                    
                    if myType == "checkbox":
                        cBox = createCheckValue(myLabelName,self.mainLayout)
                        cBox.setChecked(bool(myValue))
                        self.dicSet[myValueName] = cBox
                        self.dicRead = cBox.isChecked()
                    
                    elif myType == "label":
                        label = createLabelValue(myLabelName,self.mainLayout)
                        label.setText(str(myValue))
                        self.dicSet[myValueName] = label
                        self.dicRead = label.text()
        
    #Przyciski
        self.apllyButton, self.cancelButton = create2Buttons(self.mainLayout)
        self.apllyButton.pressed.connect(self.buttonApplyPressed)
        self.cancelButton.pressed.connect(self.close)
    
# ---- OBSLUGA OTRZYMANYCH SYGNALOW OD UZYTKOWNIKA ----#
    '''Slot do self.apllyButton do sygnalu pressed'''
    def buttonApplyPressed(self):
        self.s_ValueChanged.emit()
        
# ---- DANE WEJSCIOWE/WYJSCIOWE ----#
    '''Zwraca wszystkie zmienne
    Argumenty:
        dic({}): pusty slownik ktory sie zapelni jesli operacja sie powiedzie
    Zwraca:
        True/False: w zaleznosci czy konwersja typow danych sie powiedzie w takim wypadku dic zostanie zapelniony
    '''
    def readValues(self,dic):
        try:
            for key in self.dicSet.keys():
                value = self.dicSet[key]
                if isinstance(value, QLineEdit):
                    test = float( value.text() )
                    dic[key] = test
                elif isinstance(value, QCheckBox):
                    test = bool( value.isChecked() )
                    dic[key] = test
            return True
        except:
            return False
       
    '''Ustawienie wszystkich zmiennych'''
    def setValues(self,dic):
        for key in dic.keys():
            #print "key", key
            if key in self.dicSet.keys():
                toSet = self.dicSet[key]
                if isinstance(toSet, QCheckBox):
                    toSet.setChecked(bool(dic[key]))
                elif isinstance(toSet, QLineEdit):
                    toSet.setText(str(dic[key]))

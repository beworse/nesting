#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import division #1/2 = 0.5
from math import * #podstawowe funkcje matematyczne
from vectors import Point, Vector #https://github.com/allelos/vectors
import copy

eps = 0.0001 #do porowniania liczb zmienno przecinkowych
floatRoundTo = '.3f' #tyle ile eps

''' Klasa, ktora zawiera w sobie wszystkie punkty figury i najistotniejsze funkcje. 
Najwazneijsza funkcjonalnosc
- podzial figury na trojkaty
- znalezienie min i max polozenie (na zasadzie prostokata)
- obliczenie wspolcznnikow wszystkich prostych ( y = ax + b )
- oblicza pole figury
- detekcja kolizji z inna figura, linia
'''
class MyFigure():
#---------- INICJALIZACJA ----------# 
    '''inicjalizacja klasy
    Argumenty
        doStuf: jesli nie jest ustawiony nie oblicza parametrow, trzyma tylko punkty (ale mozna w kazdym momencie obliczyc niezbedne paretry)
    '''
    def __init__(self, points, doStuf = True, debug = []):
        self.variables(points,doStuf)
        if(len(debug) > 0): self.setDebugValues(debug)
        if(doStuf == True):
            self.triangulation()
            self.calcCentroid()
            self.findMinMax()
            self.calcLinearParms()
            self.caclArea()
            self.calc90DegreesInsideFigure()

    '''zmienne wykorzystywane przez funkcje'''
    def variables(self, points, doStuf):
    #dane wejsciowe - punkty figury zaokraglam tutaj ze wzgledu na generator figur
        if(doStuf == True):
            fp = copy.deepcopy(points)
            for point in fp:
                point.x = float(format(float(point.x),floatRoundTo))
                point.y = float(format(float(point.y),floatRoundTo))
                point.z = float(format(float(point.z),floatRoundTo))
            self.v = fp
        else:
            self.v = points
    
    #Nr wierzcholkow majacyh 90 stopni
        self.d = []
    
    #dane po triangualizacji
        self.triangles = [] #lista punktow w kazdym trojkacie (jako pkt)
        self.nrTriangles = [] #lista punktow w kazdym trojkacie (jako nr pkt)
        
        #min i max
        self.Min = Point(0,0,0) #minimalny x i y
        self.Max = Point(0,0,0) #maksymalny x i y
        
        #centroid
        self.A = 0.0
        self.cx = 0.0
        self.cy = 0.0
        
        #detekcja na zasadzie prostych y=ax+b
        self.linearParms = [] #wszystkie wpsolczynniki prostych
        self.returnFirst=True #opcja do funkcji colisionTriangles jesli true zwraca pierwsza kolizje w innym wypadku szuka wszystkich kolizji
        
    #debugowanie (wypisywanie informacji)
        self.debugCentroid = False
        self.debugTriangulation = False
        self.debugPointInsideTriangle = False
        self.debugMinMax = False
        self.debugCalcLinearParm = False
        self.debugCalcLinearParms = False
        self.debugLineCutting = False
        self.debugColisionTriangles = False
        self.debugFind = False 

    '''funkcja pozwalajaca ustawic zmienne debugowania konkretnych funkcji
    Argument(y):
        - dic: slownik zawierajacy wszystkie parametry
            ["returnFirst"](bool): jesli jest ustawiony to gdy znajdzie kolizje nie sprawdza dalej
            ["debugCentroid"](bool): wypisywanie informacji podczas obliczenia centroida
            ["debugTriangulation"](bool): wypisywanie informacji podczas dzielenia figury na trojkaty
            ["debugPointInsideTriangle"](bool): wypisywanie informacji podczas sprawdzania czy punkt jest w trojkacie (uzywane do sprawdzenia czy figura jest w figurze)
            ["debugMinMax"](bool): wypisywanie informacji podczas obliczenia minimum i maksimum
            ["debugCalcLinearParm"](bool): wypisywanie informacji podczas obliczenia pojedynczego parametru A i B
            ["debugCalcLinearParms"](bool): wypisywanie informacji podczas obliczenia wszystkich parametrow (ale nie pojedynczych)
            ["debugLineCutting"](bool): wypisywanie informacji podczas sprawdznia czy linie sie przecinaja
            ["debugColisionTriangles"](bool): wypisywanie informacji na temat kolizji dwoch trojkatow
    '''
    def setDebugValues(self,dic):
        self.returnFirst = dic["returnFirst"]
        self.debugCentroid = dic["debugCentroid"]
        self.debugTriangulation = dic["debugTriangulation"]
        self.debugPointInsideTriangle = dic["debugPointInsideTriangle"]
        self.debugMinMax = dic["debugMinMax"]
        self.debugCalcLinearParm = dic["debugCalcLinearParm"]
        self.debugCalcLinearParms = dic["debugCalcLinearParms"]
        self.debugLineCutting = dic["debugLineCutting"]
        self.debugColisionTriangles = dic["debugColisionTriangles"]
        self.debugFind = dic["debugFind"]
        
#---------- KATY W FIGURZE---------# 
    '''dlugosc wektora'''
    def vectorLength(self,A):
        return ((A.x**2)+(A.y**2))**0.5
        
    '''iloczyn skalrany'''
    def dotProduct(self,A,B):
        return (A.x * B.x)+(A.y * B.y)
    
    '''Oblicza kat miedzy wektorami'''
    def degreesBetweenVectors(self,A,B):
        down = (self.vectorLength(A) ) * ( self.vectorLength(B) ) 
        if(self.debugFind == True)and(down == 0): print "Dzielnie przez 0?"
        cosDeg =  self.dotProduct(A,B) / down
        deg = degrees(acos(radians(cosDeg))) #wyliczony kat uwaga na dogladnosc
        if(self.debugFind == True): print "cosDeg:",cosDeg , "1 - cosDeg",abs(1.0 - cosDeg)
        if(self.debugFind == True): print "deg:",deg , "90 - cosDeg",abs(90.0 - deg), "\n"
        if abs(90.0 - deg) < eps:
            return deg, True
        else:
            return deg, False
            
    '''Oblicza katy wewnatrz figury'''
    def calc90DegreesInsideFigure(self):
        self.degrees90 = [] #wyczyszczenie tablicy
         
        for i in range(0,len(self.v)-1):
            pp = self.v[i-1] #poprzedni punkt
            p = self.v[i] #badany punkt
            np = self.v[i+1] #nastepny punkt
            deg, test = self.degreesBetweenVectors(Point(p.x - pp.x, p.y - pp.y,0.0),\
                                                   Point(p.x - np.x, p.y - np.y,0.0))
            if(test == True): self.degrees90.append(i)
            
        #ostatni punkt
        pp = self.v[ (len(self.v) - 2) ] #poprzedni punkt
        p = self.v[ len(self.v) - 1 ] #badany punkt
        np = self.v[0] #nastepny punkt
        deg, test = self.degreesBetweenVectors(Point(p.x - pp.x, p.y - pp.y,0.0),\
                                               Point(p.x - np.x, p.y - np.y,0.0))
        if(test == True): self.degrees90.append(len(self.v)-1)
        
        if(self.debugFind == True): print "znalezione nr wierzcholkow w self.v", self.degrees90
    
#---------- POLE FIGURY---------# 
    '''Obliczanie dlugosci odcinka AB'''
    def lineLength(self,A,B):
        return ( ( (A.x-B.x) ** 2.0 ) + ( (A.y - B.y) ** 2.0) ) ** 0.5

    '''Oblicza pole figury ze wzoru Herona'''
    def caclArea(self):
        areaSum = 0.0
        for triangle in self.triangles:
            A,B,C = triangle
            a = self.lineLength(A,B)
            b = self.lineLength(B,C)
            c = self.lineLength(A,C)
            
            #obliczenie pola
            s = (a+b+c) / 2.0
            area = (  s * (s-a) * (s-b) * (s-c) ) ** 0.5
            areaSum += area
            
            #obliczenie katow
        
        self.areaSum = areaSum
    
#---------- OBLICZENIE PARAMETROW---------# 
    '''Oblicza srodek geometryczny figury (sluzy do obracania figury)'''
    def calcCentroid(self):
    #https://en.wikipedia.org/wiki/Centroid#Centroid_of_polygon
        cx = 0.0
        cy = 0.0
        A = 0.0
        
        for i in range(0, len(self.v)-1):
            tmp = (self.v[i].x * self.v[i+1].y ) - ( self.v[i+1].x * self.v[i].y )  #wspolne dla cx i cy
            cx += ( ( self.v[i].x + self.v[i+1].x) * ( tmp ) )
            cy += ( (self.v[i].y + self.v[i+1].y) * ( tmp ) )
            A += ( ( self.v[i].x * self.v[i+1].y) - ( self.v[i+1].x * self.v[i].y ) )
        
        tmp = (self.v[-1].x * self.v[0].y ) - ( self.v[0].x * self.v[-1].y )  #wspolne dla cx i cy
        cx += ( ( self.v[-1].x + self.v[0].x) * ( tmp ) )
        cy += ( (self.v[-1].y + self.v[0].y) * ( tmp ) )
        A += ( ( self.v[-1].x * self.v[0].y) - ( self.v[0].x * self.v[-1].y ) )
        
        
        self.A = A/(2.0) 
        if(A != 0):
            self.cx = cx / ( 6 * self.A)
            self.cy = cy / ( 6 * self.A)
        else:
            self.cx = 0
            self.cy = 0
        
        if(self.debugCentroid): print "Centroid:", self.cx, self.cy

    '''oblicza prametry dla punktow A,B'''
    def calcLinearParm(self,A,B):
        a = B.x - A.x
        
        if(a != 0): #da sie obliczyc parametry a i b?
            a = (B.y - A.y) / a
            b = A.y - (a*A.x)
            if(self.debugCalcLinearParm == True): print "Udalo sie znalezc parmetry dla punktow:", A , " oraz ", B , " a = ", a , "b = ", b
            return True,a,b #udalo sie, parametry prostej to a b
        
        b = B.y - A.y
        
        if(b == 0) and ( a == 0): #Ten sam pkt ?
            if(self.debugCalcLinearParm == True): print "Punkty ", A , " oraz ", B , " sa takie same"
            return False, 0,0 #To ten sam punkt
        
        else: #prosta rownolegla do OY
            if(self.debugCalcLinearParm == True): print "Nie udalo sie znalezc parmetrow dla punktow:", A , " oraz ", B , " bo jest to prosta rownolegla do OY"
            return False, 0, 0 

    '''oblicza parametry dla wszystkich trojkatow'''
    def calcLinearParms(self):
        self.linearParms = []
        
        if(self.debugCalcLinearParms == True): print "calcLinearParms BEGIN"
        for triangle in self.triangles:
            
            wsp = {}
            A,B,C = triangle[0], triangle[1], triangle[2] #punkty
            if(self.debugCalcLinearParms == True): print "Obliczam prametry dla A,B,C: ", A, B, C
            
            wsp['AB'] = self.calcLinearParm(A,B)
            wsp['AC'] = self.calcLinearParm(A,C)
            wsp['BC'] = self.calcLinearParm(B,C)
            
            if(self.debugCalcLinearParms == True): print "Wynik obliczen:", wsp, "\n"
            
            self.linearParms.append(wsp)
            
        if(self.debugCalcLinearParms == True): print "calcLinearParms END"

    '''znajduje najmniejsze x, y oraz najwieksze x,y '''
    def findMinMax(self):
        for i in range(0, len(self.v)):
            if i == 0:
                self.Min.x = self.v[i].x
                self.Max.x = self.v[i].x
                
                self.Min.y = self.v[i].y
                self.Max.y = self.v[i].y
                
            else:
                vx = self.v[i].x
                vy = self.v[i].y
                
                if self.Min.x > vx:
                    self.Min.x = vx
                if self.Max.x < vx:
                    self.Max.x = vx
                    
                if self.Min.y > vy:
                    self.Min.y = vy
                if self.Max.y < vy:
                    self.Max.y = vy
                    
        if(self.debugMinMax): print "Min:", self.Min, " Max:", self.Max

#---------- EAR CLIPPING (PODZIAL FIGURY NA TROJKATY) ---------# 
    '''zmiana figury na trojkaty'''
    def triangulation(self):
        
        c = [] #nr wierzcholkow wypuklych
        r = [] #nr wierzcholkow wkleslych
        p = [] #lista wierzkocholkow
        e = [] #lista uszu
        triangles = [] #lista punktow w kazdym trojkacie (jako pkt)
        nrTriangles = [] #lista punktow w kazdym trojkacie (jako nr pkt)
        
    #przyporzadkowanie wierzcholkow jako wklese lub wypukle
        for n, i in enumerate(self.v):
            
            v0 = Vector.from_points(self.v[n], self.v[(n - 1) % len(self.v)])
            v1 = Vector.from_points(self.v[n], self.v[(n + 1) % len(self.v)])
            #print v0, v1,
            ang = degrees(atan2(v0.y,v0.x) - atan2(v1.y, v1.x))
            while ang < 0:
                ang += 360.0
            #print ang
            if ang < 180.0:
                c.append(n)
            else:
                r.append(n)
            p.append(n)
                
        i = 0 #wybrany wierzcholek do testow
        while len(p) > 3:
            w = p[i % len(p)] #nr badanego wierzcholka
            
            if(self.debugTriangulation): print "Dostepne wierzcholki:", p
            if(self.debugTriangulation): print "badany wierzcholek:", w
            
            if w not in r:
                pw = p[ (i-1) % len(p) ] #nr poprzedniego wierzcholka
                pn = p[ (i+1) % len(p) ] #nr nastepnego wierzcholka
                
                flagPointInside = False #czy w srodku trojkata jest punkt
                
                for nr, point in enumerate(self.v):
                    if( nr != pw) and (nr != w) and (nr != pn):
                        if(self.debugTriangulation): print "nr:",nr, pw, w , pn,"punkty:", self.v[pw], self.v[w], self.v[pn], point
                        
                        flagPointInside = self.checkIfPointInsideTriangle(self.v[pw], self.v[w] , self.v[pn], point )
                    
                        if (flagPointInside == True):
                            if(self.debugTriangulation): print "W srodku trojkata o wierzcholkach: ", pw, w, pn
                            if(self.debugTriangulation): print "lezy punkt:", nr
                            break
                        
                if ( flagPointInside == False ):
                    if(self.debugTriangulation): print ""
                    if(self.debugTriangulation): print "Do listy trojkatow dodaje:", self.v[pw] , self.v[w] , self.v[pn]
                    if(self.debugTriangulation): print "Do listy trojkatow dodaje:", pw , w , pn
                    
                    nrTriangles.append([pw,w,pn])
                    e.append(w)
                    triangles.append( [ self.v[pw] , self.v[w] , self.v[pn] ] )
                    del p[i % len(p)] #usuniecie wybranego wierzcholka
            
            else:
                if(self.debugTriangulation): print "wierzcholek ", w , " nie moze zostac wykorzystany, ze wzgledu na to, ze zostal zakwalifikowany jako wypukly"
            
            i += 1
            
        nrTriangles.append( p )
        triangles.append( [ self.v[p[0]], self.v[p[1]] , self.v[p[2]] ])
        
        self.triangles = triangles
        self.nrTriangles = nrTriangles
        
        if(self.debugTriangulation):
            print "triangulation:"
            for i in enumerate(triangles):
                print i

#---------- RZESUWANIE I ROTACJA FIGURY---------# 
    '''przesuniecie figury o wektor'''
    def move(self,dx,dy):
        for vector in self.v:
            vector.x += dx * 1.0
            vector.y += dy * 1.0
        
        self.cx += dx 
        self.cy += dy 
        
        self.findMinMax()
        self.calcLinearParms()

    '''przesuwa poczatek figury do punktu(dx,dy)'''
    def moveTo(self,dx,dy): 
        self.move(dx - self.Min.x,dy - self.Min.y)

    '''obraca punktu o dany stopein'''
    def rotatePoint(self, p, degree): 
        s = sin(degree)* 1.0
        c = cos(degree)* 1.0
        
        #zmiana ukladu wsp
        p.x -= self.cx;
        p.y -= self.cy;
        
        #obrot
        xnew = p.x * c - p.y * s;
        ynew = p.x * s + p.y * c;
        
        #powrot do ukladu wsp
        p.x = xnew + self.cx;
        p.y = ynew + self.cy;

    '''Obraca figure o dany stopien'''
    def rotate(self, degree):
        degree = radians(degree*1.0)
        for point in self.v:
            self.rotatePoint(point, degree)
        self.findMinMax()

#---------- KOLIZJA PUNKTU Z TROJKATEM---------# 
    '''funkjca pomocnicza do checkIfPointInsideTriangle'''
    def Sign(self, p1, p2, p3):
        return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y)

    '''Sprawdza czy punkt znajduje sie w danym trojkacie'''
    def checkIfPointInsideTriangle(self, v1, v2, v3,pt):
        #print v1,v2,v3,pt
        b1 = self.Sign(pt, v1, v2) < 0.0
        b2 = self.Sign(pt, v2, v3) < 0.0
        b3 = self.Sign(pt, v3, v1) < 0.0
        if(self.debugPointInsideTriangle): print b1,b2,b3
        return bool(((b1 == b2) and (b2 == b3)))
       
    '''Sprawdza czy trojkat znajduje sie w trojkacie'''
    def isTriangleInsideTriangle(self, A,B,C, E,F,G):
        test = self.checkIfPointInsideTriangle(A,B,C,E) and self.checkIfPointInsideTriangle(A,B,C,F) and self.checkIfPointInsideTriangle(A,B,C,G)  
        test = test or self.checkIfPointInsideTriangle(E,F,G,A) and self.checkIfPointInsideTriangle(E,F,G,B) and self.checkIfPointInsideTriangle(E,F,G,A)  
        return test
        
#---------- KOLIZJA PROSTEJ Z PROSTA---------# 
    '''sprawdza czy Px lezy miedzy Ax i Bx (jeden wymiar moze byc tez y,z)'''
    def isBetween(self,Ax,Bx,Px): #)
        if (abs(Ax - Bx) < eps):
            if(self.debugLineCutting == True): print "0"
            if (abs(Px - Ax) < eps):
                return True
            else:
                return False    
        elif( abs(Ax - Px) < eps):
            if(self.debugLineCutting == True): print "3"
            return True
        
        elif( abs(Bx - Px) < eps):
            if(self.debugLineCutting == True): print "4"
            return True
            
        elif (Ax < Bx):
            if(self.debugLineCutting == True): print "1"
            if (Px >= Ax) and (Px <= Bx):
                return True
            else:
                return False
        elif (Ax > Bx):
            if(self.debugLineCutting == True): print "2"
            if (Px >= Bx) and (Px <= Ax):
                return True
            else:
                return False
        
    '''spawdza czy linie sie przecinaja
    Argumentu:
        punkty A,B,E,F
        parametry prostych aAB, bAB, aEF, bEF
        true/false z calcLinearParm tAB, tEF
    '''
    def lineCutting(self, A,B, aAB,bAB, tAB, E,F, aEF, bEF, tEF): #funkcja ktora sprawdza czy linie sie przecinaja
        if(self.debugLineCutting == True):
            print "Wspolczynnik aAB", aAB, "bAB:", bAB, "tAB:", tAB
            print "Wspolczynnik aEF", aEF, "bEF:", bEF, "tEF:", tEF
             
        if(tAB == True)and(tEF == True): #prametry a sa mozliwe do obliczenia
            X = aAB - aEF
            if(abs(X) > eps): #proste sie przecinaja
                X = (bEF-bAB) / (aAB - aEF)
                #Y = (( aAB * bEF) - (aEF*bAB) ) / (aAB - aEF)
                Y = aAB*X + bAB
                P = Point(X,Y,0)
                
                test = self.isBetween(A.x,B.x, X) and self.isBetween(A.y,B.y,Y) and self.isBetween(E.x,F.x,X) and self.isBetween(E.y,F.y,Y) 
                
                if(self.debugLineCutting == True):
                    print "W1 Proste sie przecinaja w punkcie", P
                    if(test): print "punkt lezy pomiedzy A i B", A, B, "i P:", P
                    else: print "punkt nie lezey pomiedzy A i B" 
                    
                return test, P
            
            Y = bAB - bEF
            P = Point(0,0,0)
            if( abs(Y) > eps):
                if(self.debugLineCutting == True): print "W2 Proste sie nie przecinaja, bo sa rownolegle"
                return False, P
            else:
                test = self.isBetween(A.x,B.x,P.x) and self.isBetween(A.y,B.y,P.y) and self.isBetween(E.x,F.x,P.x) and self.isBetween(E.y,F.y,P.y) 
                
                if(self.debugLineCutting == True):
                    print "To ta sama prosta"
                    if(test): print "punkt lezy pomiedzy A i B"
                    else: print "punkt nie lezey pomiedzy A i B" 
                    
                return test, P#to ta sama prosta
                
        
    #jedna z prostych jest rownolegla do 0Y
        if(tAB == False)and(tEF == True): #AB to prosta rownolegla do OY
            X = A.x
            Y = (aEF*X)+bEF
            P = Point(X,Y,0)
            
            test = self.isBetween(A.x,B.x,P.x) and self.isBetween(A.y,B.y,P.y) and self.isBetween(E.x,F.x,P.x) and self.isBetween(E.y,F.y,P.y) 
            
            if(self.debugLineCutting == True): 
                print "W3 Jedna z prostych jest rownolegla do OY, przecinaja sie w punkcie",P
                if(test): print "punkt lezy pomiedzy A i B"
                else: print "punkt nie lezey pomiedzy A i B" 
                    
            return test,P
            
        elif(tAB == True)and(tEF == False): #EF to prosta rownolegla do OY
            X = E.x
            Y = (aAB*X)+bAB
            P = Point(X,Y,0)
            
            test = self.isBetween(A.x,B.x,P.x) and self.isBetween(A.y,B.y,P.y) and self.isBetween(E.x,F.x,P.x) and self.isBetween(E.y,F.y,P.y) 

            if(self.debugLineCutting == True): 
                print "W4 Jedna z prostych jest rownolegla do OY, przecinaja sie w punkcie",P
                if(test): print "punkt lezy pomiedzy A i B"
                else: print "punkt nie lezey pomiedzy A i B" 
                    
            return test,P
            
    #obie proste sa rownolegla do 0Y
        P = Point(0,0,0)
        if( abs(A.x - E.x) < eps) or ( abs(B.x - E.x)< eps):
            test = (self.isBetween(A.y,B.y,E.y) and self.isBetween(A.y,B.y,F.y)) or (self.isBetween(E.y,F.y,A.y) and self.isBetween(E.y,F.y,B.y)) 
            
            if(self.debugLineCutting == True): 
                print "W5 To ta sama prosta rownolegla do OY"
                if(test): print "punkt lezy pomiedzy A i B"
                else: print "punkt nie lezey pomiedzy A i B" 
            
            return test,P
        else:
            if(self.debugLineCutting == True): print "W6 Proste sa ronolegle do siebie i do OY"
            return False,[]
        
        return False, [] #imposible
        
#---------- KOLIZJA FIGURY Z FIGURA---------# 
    '''kolizja dwoch figur podzielonych na trojkaty''' 
    def colisionTriangles(self,figure):
        self.calcLinearParms()
        
    #parametr returnFirst gdy jest ustawiony na true od razu jak znajduje kolizje zwraca true i punkty kolizji w innym wypadku poszukuje dalszych punktow
        returnFirst=self.returnFirst
    
    #wszystie trojkaty figur
        at1 = self.triangles #all triangles from figure 1
        at2 = figure.triangles
        
    #wszystkie wpsolczynniki prostych
        alp1 = self.linearParms #all line parameters from figure 1
        alp2 = figure.linearParms
    
    #zmienne
        collisionPoints = []
        triangleInsideTriangle = False
    
        if(self.debugColisionTriangles == True): 
            print "colisionTriangles BEGIN"
            print "badane figury:\n", at1 , "\n", at2, "\n"

        for t1c in range(0,len(at1)): #t1c = triangles from figure 1 counter
            t1 = at1[t1c] #t1 = triangle from figure 1
            A1,B1,C1 = t1[0], t1[1], t1[2] #kolejnosc wazana
            
            parm1 = alp1[t1c]
            AB1 = parm1['AB']
            AC1 = parm1['AC']
            BC1 = parm1['BC']
            
            for t2c in range(0,len(at2)):
                t2 = at2[t2c] #triangle from figure 2
                A2,B2,C2 = t2[0], t2[1], t2[2] #kolejnosc istotna
                
                parm2 = alp2[t2c]
                AB2 = parm2['AB']
                AC2 = parm2['AC']
                BC2 = parm2['BC']
                
            #brzydki sposob badania wszystkich kombinacji
                if(self.debugColisionTriangles == True): print "Badane prosta f1:", A1,B1 ,"wraz z parametrami:", AB1, "\nBadane prosta f2:", A2,B2 ,"wraz z parametrami:", AB2
                
                #AB1 = const
                test,point = self.lineCutting(A1,B1, AB1[1], AB1[2], AB1[0] , A2,B2, AB2[1], AB2[2], AB2[0]) 
                if(test == True):
                    collisionPoints.append([A1,B1,A2,B2,point])
                    if(returnFirst == True): return test, collisionPoints
                   
                test,point = self.lineCutting(A1,B1, AB1[1], AB1[2], AB1[0] , A2,C2, AC2[1], AC2[2], AC2[0]) 
                if(test == True):
                    collisionPoints.append([A1,B1,A2,C2,point])
                    if(returnFirst == True): return test, collisionPoints
                
                test,point = self.lineCutting(A1,B1, AB1[1], AB1[2], AB1[0] , B2,C2, BC2[1], BC2[2], BC2[0]) 
                if(test == True):
                    collisionPoints.append([A1,B1,B2,C2,point])
                    if(returnFirst == True): return test, collisionPoints
                   
                #AC1 = const
                test,point = self.lineCutting(A1,C1, AC1[1], AC1[2], AC1[0] , A2,B2, AB2[1], AB2[2], AB2[0]) 
                if(test == True):
                    collisionPoints.append([A1,C1,A2,B2,point])
                    if(returnFirst == True): return test, collisionPoints
                   
                test,point = self.lineCutting(A1,C1, AC1[1], AC1[2], AC1[0] , A2,C2, AC2[1], AC2[2], AC2[0]) 
                if(test == True):
                    collisionPoints.append([A1,C1,A2,C2,point])
                    if(returnFirst == True): return test, collisionPoints
                
                test,point = self.lineCutting(A1,C1, AC1[1], AC1[2], AC1[0] , B2,C2, BC2[1], BC2[2], BC2[0]) 
                if(test == True):
                    collisionPoints.append([A1,C1,B2,C2,point])
                    if(returnFirst == True): return test, collisionPoints
                    
                #BC1 = const
                test,point = self.lineCutting(B1,C1, BC1[1], BC1[2], BC1[0] , A2,B2, AB2[1], AB2[2], AB2[0]) 
                if(test == True):
                    collisionPoints.append([B1,C1,A2,B2,point])
                    if(returnFirst == True): return test, collisionPoints
                   
                test,point = self.lineCutting(B1,C1, BC1[1], BC1[2], BC1[0] , A2,C2, AC2[1], AC2[2], AC2[0]) 
                if(test == True):
                    collisionPoints.append([B1,C1,A2,C2,point])
                    if(returnFirst == True): return test, collisionPoints
                
                test,point = self.lineCutting(B1,C1, BC1[1], BC1[2], BC1[0] , B2,C2, BC2[1], BC2[2], BC2[0]) 
                if(test == True):
                    collisionPoints.append([B1,C1,B2,C2,point])
                    if(returnFirst == True): return test, collisionPoints
                
            #koniec brzydkiego sposob badania wszystkich kombinacji
                
            #spradzenie czy trojkat jest w trojkacie
                test = self.isTriangleInsideTriangle(A1,B1,C1,A2,B2,C2)
                
                if(test == True):
                    if(self.debugColisionTriangles == True): print "Trojkat ABC zawarty EFG lub EFG zawarty w ABC"
                    if(returnFirst == True): return test, collisionPoints
                    
                triangleInsideTriangle = triangleInsideTriangle or test
                
                if(self.debugColisionTriangles == True): print "" #koniec badania kolizji dla t1
            
        if(self.debugColisionTriangles == True): print "colisionTriangles END \n"
        
    #jesli returnFirst=True lub brak kolizji
        if(triangleInsideTriangle == True):
            return True, collisionPoints
        elif(len(collisionPoints) == 0):
            return False, []
        else:
            return True, collisionPoints
    

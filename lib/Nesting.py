#!/usr/bin/python
# -*- coding: utf-8 -*-

#import os
#import sys
from __future__ import division #0/2 = 1.5 (domyslny wynik 0)
import copy #potrzebne do kopiowania
from vectors import Point #do punktow x,y
from MyFigure import MyFigure #obiekt na ktorym bede dzialac
import math

class Nesting():
# ---- INICJALIZACJA KLASY ----#
    '''#inicjalizacja obiektu (czyli tylko debugVariables)'''
    def __init__(self): 
        self.variables() #inicjalizacja zmiennych 
        self.debugVariables() #pomocne przy debugowaniu
        
    '''opis zmiennych w jednym miejscu'''
    def variables(self): #
    #kopia zapasowa
        self.beforeNestingfigures = [] #lista figur przed nestingiem
        
    #parametry przykladania
        self.nestingSpace = 0.5 #odstep od badanego punktu
        self.space = 1.0 #minimalny odpset od punktu (wrazie braku znalezienia nestingu zwieksza ten parametr)
        self.angle = 10.0 
        self.nestingMoveAlgorithm = True #uzyc algorytmu Move?
        self.nestingCircleAlgorithm = False #uzyc algorytmu Cicrcle?
        self.circleAngle = 10
        
    #parametry dopasowania pola
        self.useArea = False #flaga wlaczajaca dopasowania do pola
        self.Min = Point(0,0,0) #najmniejsze x oraz y w znestingowanych figurach
        self.Max = Point(0,0,0) #najwiekszy x oraz y w znestingowanych figurach
        self.bestArea = 0.0 #najlepsze znane pole
        self.bestPlace = None #najlepsza znaleziona pozycja
        
    #parametry dopasowania centroidu
        self.useCentroid = True  #flaga wlaczaja dopasowanie do centroidu
        self.cxCentroid = None
        self.cyCentroid = None
        self.bestCentroid = None #najelpszu znaleziony centroid
        
    #parametry dopasowania pola i centroidu
        self.epsCentroid = 0.01
        
    #pozostale parametry
        self.table = [] #stol na ktorym beda ukladane figury
        self.maxtablei = 3 #maksymalna liczba powtorzen dla roznych odleglosci
        
    '''zmienne klasy sluzace do debugowania'''
    def debugVariables(self):
        self.debugNestingData = False
        self.debugMoveToPoint = False
        self.debugMoveToPoints = False
        self.debugNestingStart = False

    def setNestingValues(self, dic):
        self.nestingMoveAlgorithm = dic["nestingMoveAlgorithm"]  #uzyc algorytmu Move?
        self.nestingCircleAlgorithm = dic["nestingCircleAlgorithm"]  #uzyc algorytmu Cicrcle?
        self.angle = float(dic["nestingRotateValue"]) #rotacja
        self.nestingSpace = float(dic["nestingSpace"]) #przykladanie/promien
        self.useCentroid = bool(dic["useCentroid"])
        self.useArea = bool(dic["useArea"])
        self.epsCentroid = float(dic["epsCentroid"])
        self.circleAngle = float(dic["circleAngle"])
        self.table = [Point(dic["tablebeginx"], dic["tablebeginy"],0.0),\
                      Point(dic["tableendx"] , dic["tableendy"],0.0)]
        
    def setDebugValues(self,dic):
        self.debugNestingData = dic["debugNestingData"]
        self.debugMoveToPoint = dic["debugMoveToPoint"]
        self.debugMoveToPoints = dic["debugMoveToPoints"]
        self.debugNestingStart = dic["debugNestingStart"] 
        
# ---- USTAWIENIE ZMIENNYCH ----#
    def setNestingData(self, listOfFigures):
        self.beforeNestingfigures = copy.deepcopy(listOfFigures) #kopia zapasowa
        if(self.debugNestingData == True): print "Ustawione nowe dane\n odstep:", self.nestingSpace, " oraz liste figur:", self.beforeNestingfigures
        
# ---- DOPASOWANIE DO PUNKTU----#
    '''Spawdza czy warto(True) badac kolizje - wersja prosta'''
    def AABB(self, f1, f2):
    #odczytanie prostokata f1
        mif1x = f1.Min.x
        mif1y = f1.Min.y
        maf1x = f1.Max.x
        maf1y = f1.Max.y
        
    #odczytanie prostokata f2
        mif2x = f2.Min.x
        mif2y = f2.Min.y
        maf2x = f2.Max.x
        maf2y = f2.Max.y
        
    #sprwadzenie dla x
        testx = f1.isBetween(mif1x, maf1x, mif2x) or f1.isBetween(mif1x, maf1x, maf2x) or\
                f1.isBetween(mif2x, maf2x, mif1x) or f1.isBetween(mif2x, maf2x, maf1x)
               
    #sprwadzenie dla x
        testy = f1.isBetween(mif1y, maf1y, mif2y) or f1.isBetween(mif1y, maf1y, maf2y) or\
                f1.isBetween(mif2y, maf2y, mif1y) or f1.isBetween(mif2y, maf2y, maf1y)
        
        test = testx and testy
        
        return test

    '''dopasowanie na zasadzie pola trojkata'''
    def areaLikeSquar(self):
        self.f1.findMinMax() #obliczenie nowego min i max
        #obliczenie nowego minimum
        Minx = min(self.f1.Min.x, self.Min.x)*1.0
        Miny = min(self.f1.Min.y, self.Min.y)*1.0
    #obliczenie nowego maximum
        Maxx = max(self.f1.Max.x, self.Max.x)*1.0
        Maxy = max(self.f1.Max.y, self.Max.y)*1.0
    #obliczenie nowego zajetego obszaru
        newArea = abs(Maxx - Minx) * abs(Maxy - Miny)
        if(self.debugMoveToPoint): 
            print "znaleziono punkt bez kolizji o obszarze: ", newArea
            print "Min : ", self.f1.Min, "Max : ", self.f1.Max
    #sprawdzenie czy nowy znaleziony obszar jest dobry
        if( (self.bestArea == None) or (self.bestArea > newArea) ):
            self.bestPlace = copy.deepcopy(self.f1)
            self.bestArea = newArea
            if(self.debugMoveToPoint): 
                print "Podmianka\n","Nowe Pole: ",newArea,
                print "ilosc zbadanych figur: ",ii
                print "Min : ", self.f1.Min, "Max : ", self.f1.Max
        
    '''oblicza centroid kilku figur'''
    def calcMegaCentroid(self,figures):
        cx = 0.0
        cy = 0.0
        A = 0.0
        
        for figure in figures:
            for i in range(0, len(figure.v)-1):
                tmp = (figure.v[i].x * figure.v[i+1].y ) - ( figure.v[i+1].x * figure.v[i].y )  #wspolne dla cx i cy
                cx += ( ( figure.v[i].x + figure.v[i+1].x) * ( tmp ) )
                cy += ( (figure.v[i].y + figure.v[i+1].y) * ( tmp ) )
                A += ( ( figure.v[i].x * figure.v[i+1].y) - ( figure.v[i+1].x * figure.v[i].y ) )
            
            tmp = (figure.v[-1].x * figure.v[0].y ) - ( figure.v[0].x * figure.v[-1].y )  #wspolne dla cx i cy
            cx += ( ( figure.v[-1].x + figure.v[0].x) * ( tmp ) )
            cy += ( (figure.v[-1].y + figure.v[0].y) * ( tmp ) )
            A += ( ( figure.v[-1].x * figure.v[0].y) - ( figure.v[0].x * figure.v[-1].y ) )
        
        A = A/(2.0) 
        if(A != 0):
            cx = cx / ( 6 * A)
            cy = cy / ( 6 * A)
        else:
            cx = 0
            cy = 0
        #print "cx:",cx,"cy",cy
        return cx, cy, A
    
    '''dopasowanie na zasadzie centroida'''
    def areaLikeCentroid(self):
        tmpList = copy.deepcopy(self.anf)
        tmpList.append(self.f1)
        ncx, ncy, Area = self.calcMegaCentroid(tmpList)
        newArea = abs(ncx - self.cxCentroid) + abs(ncy - self.cyCentroid)
        
        if( (self.bestCentroid == None) or (self.bestCentroid  > newArea) ):
            self.bestCentroid = newArea
            self.bestPlace = copy.deepcopy(self.f1)
    
    '''dopasowanie mieszane'''
    def areaLikeCentroidAndArea(self):
    #dopasowanie przy uzyciu pola
        self.f1.findMinMax() #obliczenie nowego min i max
        #obliczenie nowego minimum
        Minx = min(self.f1.Min.x, self.Min.x)*1.0
        Miny = min(self.f1.Min.y, self.Min.y)*1.0
        #obliczenie nowego maximum
        Maxx = max(self.f1.Max.x, self.Max.x)*1.0
        Maxy = max(self.f1.Max.y, self.Max.y)*1.0
        #obliczenie nowego zajetego obszaru
        newAreaA = abs(Maxx - Minx) * abs(Maxy - Miny)
        
    #dopasowanie przy uzyciu centroida
        tmpList = copy.deepcopy(self.anf)
        tmpList.append(self.f1)
        ncx, ncy, Area = self.calcMegaCentroid(tmpList)
        newAreaC = abs(ncx - self.cxCentroid) + abs(ncy - self.cyCentroid)
        tmpList = copy.deepcopy(self.anf)
        tmpList.append(self.f1)
        ncx, ncy, Area = self.calcMegaCentroid(tmpList)
        newAreaC = abs(ncx - self.cxCentroid) + abs(ncy - self.cyCentroid)
        
    #warunki wlasciwe
        if( (self.bestCentroid == None) or (self.bestArea  == None) ):
            self.bestArea = newAreaA
            self.bestCentroid = newAreaC
            self.bestPlace = copy.deepcopy(self.f1)
        elif (self.bestCentroid - newAreaC) < self.epsCentroid: #centroid jest zaniedbywalny:
            if( (self.bestArea > newAreaA) ):
                self.bestArea = newAreaA
                self.bestCentroid = newAreaC
                self.bestPlace = copy.deepcopy(self.f1)
        elif( self.bestCentroid > newAreaC):
                self.bestArea = newAreaA
                self.bestCentroid = newAreaC
                self.bestPlace = copy.deepcopy(self.f1)
                
# ---- CZESC WLASCIWA ----#
    '''x,y obliczony wektor przesuniecia figury'''
    def moveToPoint(self,x,y):
        self.f1.move(x,y)
        self.f1.calcLinearParms() #obliczenie parametrow do kolizji
        self.f1.findMinMax()
    
    #sprawdzenie czy wchodzi do obszaru robczego
        if( len(self.table)>0):
            if not(\
                    (self.f1.Min.x > self.table[0].x) and (self.f1.Min.y > self.table[0].y)and\
                    (self.f1.Max.x < self.table[1].x) and (self.f1.Max.y < self.table[1].y)\
                  ):
                return
        
    #sprawdzenie czy wystepuje kolizja
        ii = 0
        for figure in self.anf: 
            ii+=1
            if(self.AABB(figure,self.f1) == True):
                colision, tmp = figure.colisionTriangles(self.f1)
            else:
               colision = False
                
            if(colision == True): break
        
        if(colision == False):
            if(self.useCentroid == True)and(self.useArea == True):
                self.areaLikeCentroidAndArea()
            elif(self.useCentroid == True):
                self.areaLikeCentroid()
            elif(self.useArea == True):
                self.areaLikeSquar()
            
    '''przysuwa figure z kazdej strony do punktu uzwya funkcji self.moveToPoint()'''
    def moveToPoints(self):
        if(self.debugMoveToPoints): print "prawo"
        self.moveToPoint(self.p0.x + self.space , self.p0.y) #prawo
        
        if(self.debugMoveToPoints): print "prawo gora"
        self.moveToPoint(self.p0.x + self.space , self.p0.y + self.space) #prawp gora
        
        if(self.debugMoveToPoints): print "gora"
        self.moveToPoint(self.p0.x, self.p0.y + self.space) #gora
        
        if(self.debugMoveToPoints): print "lewo gora"
        self.moveToPoint(self.p0.x - self.space , self.p0.y + self.space) #lewo gora
        
        if(self.debugMoveToPoints): print "lewo"
        self.moveToPoint(self.p0.x - self.space , self.p0.y) #lewo
        
        if(self.debugMoveToPoints): print "lewo dol"
        self.moveToPoint(self.p0.x - self.space , self.p0.y - self.space) #lewo dol
        
        if(self.debugMoveToPoints): print "dol"
        self.moveToPoint(self.p0.x, self.p0.y - self.space) #dol
        
        if(self.debugMoveToPoints): print "dol prawo"
        self.moveToPoint(self.p0.x + self.space, self.p0.y - self.space) #dol prawo
       
    '''krazy wokol podanego punktu'''
    def longerMoveToPoint(self):
        bp = copy.deepcopy(self.p0) #begin point
        ba = 0.0 #kat poczatkowy
    
        while(ba < 360):
            x = self.p0.x - self.f1.v[self.i].x + self.space + math.cos(math.radians(ba))            
            y = self.p0.y  - self.f1.v[self.i].y + self.space + math.cos(math.radians(ba))  
            self.moveToPoint(x,y)
            ba += self.circleAngle
            
    '''Przesywa pierwsza figure w odpowiednie miesjce'''
    def moveFirstFigure(self): 
        if ( len(self.table) != 0 ):
            for idf,f in enumerate(self.bnf):
                if(len(f.degrees90) > 0):
                    for idd,iv in enumerate(f.degrees90):
                        
                        if(iv != (len(f.v)-1)):
                            p = f.v[iv+1] 
                        else:
                            p = f.v[iv-1]
                        
                    #tak sie tego nie robi
                        d = 1.0
                        while( abs(d) > 0.01):
                            x = self.table[0].x - f.v[iv].x
                            y = self.table[0].y - f.v[iv].y
                            f.move(x,y)
                            d = math.degrees(math.atan2( self.table[0].x-p.x , f.vectorLength(p) ))
                            f.rotate(d)
                            print d
                        
                        
                    #przesunie wierzcholka do 0,0
                        x = self.table[0].x - f.Min.x + 1.0
                        y = self.table[0].y - f.Min.y + 1.0
                        f.move(x,y)
                        f.findMinMax()
                        print "Po obrocie i przesunieciu:", f.v[iv]
                        
                       
                        
                    #sprawdzenie czy nie wystaje
                        test =  (\
                                    (f.Min.x > self.table[0].x) and (f.Min.y > self.table[0].y)and\
                                    (f.Max.x < self.table[1].x) and (f.Max.y < self.table[1].y)\
                                )
                        
                        if(test == True):
                            f.calcLinearParms()
                            self.anf.append(copy.deepcopy(f))
                            del self.bnf[idf]
                            return
                        
        print ":("
        f = self.bnf[0]
        x = self.table[0].x - f.Min.x 
        y = self.table[0].y - f.Min.y 
        f.move(x,y)
        self.anf.append(copy.deepcopy(f))
        del self.bnf[0]
       
    '''Sorotowanie figur wg ilosci wierzcholkow'''
    def sortListOfFigures(self):
        self.beforeNestingfigures = sorted( self.beforeNestingfigures, key = lambda figure: len(figure.v) , reverse=True)
            
    '''Uklada figury'''
    def nestingStart(self):
        if(len(self.beforeNestingfigures) > 1):
            print "BEGIN nestingStart"
            self.sortListOfFigures() #posortowanie figur od najwiekszej do najmniejszej
        #reset zmiennychself.bestArea = 0.0
            self.anf = [] #after nesting figures (figury po nestingu)
            self.cnf = [] #figury ktorych nie udalo sie znestignowac
            self.bnf = copy.deepcopy(self.beforeNestingfigures) #before nesting figures 
            
            
            #self.anf.append(copy.deepcopy(self.bnf[0]))
            self.moveFirstFigure()
            
            self.Min = Point(self.anf[0].Min.x, self.anf[0].Min.y, 0.0) 
            self.Max = Point(self.anf[0].Max.x, self.anf[0].Max.y, 0.0)
            
            
            while(len(self.bnf)>0): #poczatek glowna petla
            #zmienne
                #wybrana figura
                self.f1 = copy.deepcopy(self.bnf[0])
                
                #dopasowanie wzgledem pola
                self.bestArea = None #najlepsze znalezione pole dla figur
                self.bestPlace = None #najlepsze znalezione miejsce
                self.space = 0.0 #odsetp od punktu
                
                
                #dopasowanie wzgledem centroidu
                self.cxCentroid , self.cyCentroid, Area = self.calcMegaCentroid(self.anf)
                self.bestCentroid = None
                    
                it = 0;
                while(self.bestArea == None)and(self.bestCentroid == None)and(it != self.maxtablei): #poczatek petli zabezpieczajacej
                    if(self.debugNestingStart): print "Ukladam figure", len(self.anf)
                    self.space += self.nestingSpace
                    self.currentAngle = 0.0 #kat poczatkowy
                    while(self.currentAngle < 360):#poczatek obracanie
                        if(self.debugNestingStart): print "Badam figure obrocona o", self.currentAngle
                        self.f1.rotate(self.currentAngle)
                        self.f1.calcLinearParms()
                        for self.f0 in self.anf: #poczatek dla kazdej nieruchomej figury
                            self.f0.calcLinearParms() #teoretycznie nie powinieniem musiec tego liczyc
                            j=0
                            for self.p0 in self.f0.v: #poczatek dla kazdego punktu nieruchomej figury
                                j+=1
                                for self.i in range(0,len(self.f1.v)): #poczatek dla kazdego nr wierzcholka ruchomej figury
                                    #fcopy = copy.deepcopy(self.f1)
                                    if( self.nestingMoveAlgorithm == True ):
                                        self.moveToPoint(self.p0.x - self.f1.v[self.i].x + self.space, self.p0.y - self.f1.v[self.i].y)
                                        self.moveToPoint(self.p0.x - self.f1.v[self.i].x + self.space, self.p0.y - self.f1.v[self.i].y + self.space)
                                        self.moveToPoint(self.p0.x - self.f1.v[self.i].x - self.space, self.p0.y - self.f1.v[self.i].y + self.space)
                                        self.moveToPoint(self.p0.x - self.f1.v[self.i].x - self.space, self.p0.y - self.f1.v[self.i].y)
                                        self.moveToPoint(self.p0.x - self.f1.v[self.i].x - self.space, self.p0.y - self.f1.v[self.i].y - self.space)
                                        self.moveToPoint(self.p0.x - self.f1.v[self.i].x, self.p0.y - self.f1.v[self.i].y - self.space)
                                    
                                    if( self.nestingCircleAlgorithm == True ):
                                        self.longerMoveToPoint()
                                    
                                #koniec dla kazdego nr wierzcholka ruchomej figury
                            #koniec dla kazdego punktu nieruchomej figury
                        #koniec dla kazdej nieruchomej figury
                        self.currentAngle += self.angle #inkrementacja rotacji
                    #koniec obracanie
                    it+=1
                #koniec petli zabezpieczajacej 
                if (self.bestPlace != None):
                    if(self.useArea  == True):
                        self.Min.x = min(self.Min.x, self.bestPlace.Min.x)*1.0
                        self.Min.y = min(self.Min.y, self.bestPlace.Min.y)*1.0
                        self.Max.x = max(self.Max.x, self.bestPlace.Max.x)*1.0
                        self.Max.y = max(self.Max.y, self.bestPlace.Max.y)*1.0
                    self.anf.append(copy.deepcopy(self.bestPlace))
                else:
                    if(self.debugNestingStart): print "Nie udalo sie ulozyc jednej z figur ze wzgledu na zbyt maly stol "
                    self.bnf[0].move(\
                                        self.table[0].x - self.bnf[0].Max.x,\
                                        self.table[0].y - self.bnf[0].Max.y\
                                    )
                    self.cnf.append(copy.deepcopy(self.bnf[0]))
                    #self.anf.append(copy.deepcopy(self.bnf[0]))
                del self.bnf[0]
                
                if(self.debugNestingStart): print "Ulozono juz ", len(self.anf), " figur\n"
            print "Znalezione pole", self.bestArea
            #koniec petli glownej
            print "END nestingStart"
        
        elif(self.debugNestingStart): print "zbyt malo zmiennych"

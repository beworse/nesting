#!/usr/bin/python
# -*- coding: utf-8 -*-

#OpenGL
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

from PyQt4.QtOpenGL import QGLWidget #Qt
from PyQt4 import QtCore
from vectors import Point, Vector #klasa ulatwiajaca zycie
from MyFigure import MyFigure #moja klasa

'''KLASA SLUZACA DO WIZUALIZACJI DANYCH'''
class PreviewGL(QGLWidget):
#---------- INICJALIZACJA ----------# 
    def __init__(self, parent=None, w=640, h=480):
        QGLWidget.__init__(self, parent)
    #minimalny rozmiar;
        self.setMinimumWidth(w)
        self.setMinimumHeight(h)
        self.variables()
        self.show()

    '''zmienne (warto zajrzec)'''
    def variables(self):
        self.divider = 10.0 #wartosc przez ktora jest dzielona wielkosc okna 
        self.currentview = [0.0, float(self.minimumWidth())/self.divider, 0.0, float(self.minimumHeight()/self.divider)] #wyswietlany fragment osi
        self.figure = [] #figury do narysowania
        self.nesting = [] 
        self.table = [] #wspolrzedne stolu
        self.zoom = 0.0 #zmienna potrzebna do zmiany wielkosci okna 
        
        #co rysuje
        self.drawPolygonsFlag = True
        self.drawTriangleFlag = True
        self.drawCentroidFlag = False
        self.drawNestingFlag  = True
        self.drawTableFalg = True
        self.drawMegaCentroidFlag = True

    '''inicjalizacja okna'''
    def initializeGL(self): 
        #gluOrtho2D(*self.currentview)               #ustawienie kamery na 1 cwiartke
        glutInit()                                  #potrzebne do rysowania tekstu
        glClearColor(1.0,1.0,1.0,.0)                #kolor tla

    '''odswiezanie sceny'''
    def paintGL(self):
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity()
        gluOrtho2D(self.currentview[0],self.currentview[1],self.currentview[2], self.currentview[3])
        glClear(GL_COLOR_BUFFER_BIT)    #wyczyszczenie bufora
        self.draw()                     #rysuje wszystkie obiekty sceny
        glFlush()                       #wymuszenie rysowania

    '''zmiana wielkosci'''
    def resizeGL(self, w, h):
        #Wyjasnienie: http://web.cse.ohio-state.edu/~wang.3602/courses/cse581-2012-spring/2D.pdf 

        #zmienne widoku
        dx = self.currentview[0] #przesuniecie widoku na x
        dy = self.currentview[2] #przesuniecie widoku na y
        self.aspect = float(w/h) #stosunek szerokosci okna do dlugosci
        
        #ustawienie odpowiednich wartosci dla gluOrtho2D
        self.currentview[0] = dx #przesuniecie okna na x
        self.currentview[1] = (w/self.divider) + dx + (self.zoom*self.aspect)  #przesuniecie i przyblizenie okna na x
        self.currentview[2] = dy #przesuniecie okna na y
        self.currentview[3] = (h/self.divider) + dy + self.zoom #przesuniecie i przyblizenie okna na x
        
        #ustawienie wielkosci okna
        glViewport(0, 0, w, h) #jaka czesc okna ma byc rysowana
        self.w, self.h = w, h  #przypisanie nowego rozmiaru okna
        self.update() #wymuszenie odswiezenia okna
        
#---------- "ZMIANA TRYBU RYSOWANAI ----------#     
    '''Przyblizanie i oddalanie gluOrtho2D'''
    def changeZoomView(self, zoom):
        self.zoom += zoom #do zmiany rozmiaru okna
        self.currentview[1] = self.currentview[1] + (zoom*self.aspect)
        self.currentview[3] = self.currentview[3] + zoom
        self.update() #odswiezenie sceny
      
    '''Przesuwanie gluOrtho2D'''
    def moveView(self,moveX,moveY):
        self.currentview[0] = self.currentview[0] + moveX
        self.currentview[1] = self.currentview[1] + moveX 
        self.currentview[2] = self.currentview[2] + moveY 
        self.currentview[3] = self.currentview[3] + moveY
        self.update() #odswiezenie sceny
        
#---------- "ZMIANA TRYBU RYSOWANAI ----------#     
    '''Rysowanie figury'''
    def setDrawingFlagPolygon(self):
        self.drawPolygonsFlag = not self.drawPolygonsFlag
        self.update()

    '''Rysowanie ear clipping'''
    def setDrawingFlagTriangle(self):
        self.drawTriangleFlag = not self.drawTriangleFlag
        self.update()

    '''Centroid figur (kazdej osobno)'''
    def setDrawingFlagCentroid(self):
        self.drawCentroidFlag = not self.drawCentroidFlag
        self.update()

    '''Wspolny centroid figur'''
    def setDrawingFlagMegaCentroid(self):
        self.drawMegaCentroidFlag = not self.drawMegaCentroidFlag
        self.update()

    '''Stol do ukladania figur'''
    def setDrawingFlagTable(self):
        self.drawTableFalg = not self.drawTableFalg
        self.update()

#---------- USTAWIENIE DANYCH DO NARYSOWWANIA ----------# 
    '''ustawienie rysowanych figur'''
    def setDrawData(self, figure): 
        self.figure = figure
        self.update()

    '''ustawienie danych'''
    def setNestingData(self, nesting):
        self.nesting = nesting
        self.update()

    '''ustawienie wielksoci stolu'''
    def setTableData(self, tab):
        self.table = tab

#---------- RYSOWANIE ----------# 
    '''glowna funkcja rysujaca'''
    def draw(self): #
        #rysowanie figur
        for figure in self.figure:
            if isinstance(figure,MyFigure): #zebezpiecznie typu zmiennej
                if(self.drawTriangleFlag):self.drawTriangles(figure)
                if(self.drawPolygonsFlag ):self.drawPolygons(figure)
                if(self.drawCentroidFlag):self.drawCentroid(figure)
                if(self.drawNestingFlag):self.drawNesting()
            else: print "wyslano nieprawidlowy typ danych do klasy PreviewGl, jest nim ", type(figure) 
            
        #rysowanie centroida wszystkich figur
        if(self.drawMegaCentroidFlag):
                cx, cy = self.calcMegaCentrooid()
                self.drawMegaCentroid(cx,cy)
        
        #rysowanie stolu        
        if(self.drawTableFalg): self.drawTable()
        
    '''rysuje linie opnegl'''
    def drawLine(self,px,py,x,y,r=0,g=0,b=0): 
        glBegin(GL_LINES) 
        glColor3d(r,g,b) #kolor linii
        glVertex2f(px, py)
        glVertex2f(x, y)
        glEnd()  

    '''zaznacza proste figur ktore sie przecinaja (fiolet) oraz zaznacza miesjce kolizji od pkt 0,0 (czerwony)'''
    def drawNesting(self):
    
        r = 255
        g = 0
        b = 255
        
        #v = self.nesting.v
        for p in self.nesting:
            if isinstance(p,MyFigure):
                for v in p.v:
                
                    self.drawLine( v[0].x , v[0].y , v[1].x , v[1].y, r , g , b )
                    self.drawLine( v[2].x , v[2].y , v[3].x , v[3].y, r , g , b )
                    self.drawLine( 0.0 , 0.0 , v[4].x , v[4].y, 255 , 0 , 0 )

    '''rysuje figure oraz jej podzial na trojkaty'''
    def drawTriangles(self, figure):
    #lista punktow
        v = figure.v
        
    #rysowanie
        for f in figure.nrTriangles: 
            A,B,C = v[f[0]], v[f[1]], v[f[2]] #rysowanie wzgledem wierzcholkow a nie punktow
            self.drawLine(A.x, A.y, B.x, B.y)
            self.drawLine(B.x, B.y, C.x, C.y)
            self.drawLine(C.x, C.y, A.x, A.y)

    '''rysuje figure bez zaznaczonych trojkatow'''
    def drawPolygons(self, figure):
        r = 0
        g = 0
        b = 255
        
        v = figure.v
        for i in range(0,len(v)-1):
            self.drawLine( v[i].x , v[i].y , v[i+1].x , v[i+1].y, r , g , b )
        self.drawLine( v[-1].x , v[-1].y , v[0].x , v[0].y, r , g , b ) #polaczenie poczatku i konca figury

    '''rysuje punkt wokol ktorego bedzie obracac figure'''
    def drawCentroid(self, figure):
    #punkty
        v = figure.v
        
    #wspolrzedne centroida
        cx = figure.cx
        cy = figure.cy
        
    #rysowanie
        for p in v:
            self.drawLine( p.x , p.y , cx , cy, 204, 0 , 102 )

    '''Oblicza wspolny centroid'''
    def calcMegaCentrooid(self):
        cx = 0.0
        cy = 0.0
        A = 0.0
        
        for figure in self.figure:
            for i in range(0, len(figure.v)-1):
                tmp = (figure.v[i].x * figure.v[i+1].y ) - ( figure.v[i+1].x * figure.v[i].y )  #wspolne dla cx i cy
                cx += ( ( figure.v[i].x + figure.v[i+1].x) * ( tmp ) )
                cy += ( (figure.v[i].y + figure.v[i+1].y) * ( tmp ) )
                A += ( ( figure.v[i].x * figure.v[i+1].y) - ( figure.v[i+1].x * figure.v[i].y ) )
            
            tmp = (figure.v[-1].x * figure.v[0].y ) - ( figure.v[0].x * figure.v[-1].y )  #wspolne dla cx i cy
            cx += ( ( figure.v[-1].x + figure.v[0].x) * ( tmp ) )
            cy += ( (figure.v[-1].y + figure.v[0].y) * ( tmp ) )
            A += ( ( figure.v[-1].x * figure.v[0].y) - ( figure.v[0].x * figure.v[-1].y ) )
        
        A = A/(2.0) 
        if(A != 0):
            cx = cx / ( 6 * A)
            cy = cy / ( 6 * A)
        else:
            cx = 0
            cy = 0
        #print "cx:",cx,"cy",cy
        return cx, cy

    '''Rysowanie wpsolnego centroida'''
    def drawMegaCentroid(self,cx,cy):
        r = 0
        g = 204
        b = 0
        for figure in self.figure:
            for p in figure.v:
                self.drawLine( p.x , p.y , cx , cy, r, g , b )   
        
    '''Rysuje stol na ktorym beda ukladane figury'''
    def drawTable(self):
        if(len(self.table) > 0):
                r = 255
                g = 0
                b = 0
                
                A,B = self.table
                self.drawLine(A.x, A.y, B.x, A.y, r, g, b)
                self.drawLine(B.x, A.y, B.x, B.y, r, g, b)
                self.drawLine(B.x, B.y, A.x, B.y, r, g, b)
                self.drawLine(A.x, B.y, A.x, A.y, r, g, b)

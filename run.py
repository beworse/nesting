#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import division #0/2 = 1.5 (domyslny wynik 0)
import sys
import os
import json #format danych testowych
import copy #potrzebne do kopiowania
import time


#Qt4
import PyQt4
from PyQt4 import QtGui, QtCore
from PyQt4.QtGui import QApplication, QMainWindow, QAction, QFileDialog, QMessageBox
from PyQt4.QtCore import QStringList, Qt

#klasy dodatkowe
from lib.vectors import Point #https://github.com/allelos/vectors

#moje klasy
from lib.PreviewGL import PreviewGL #rysowanie 
from lib.MyFigure import MyFigure #zarzadznie figura
from lib.Nesting import Nesting #ukladanie figur
from lib.Dialog import DynamicDialog
from lib.PolygonGenerator import PolygonGenerator

'''Tworzy akcje, dodaje ja do menu, pdolacza do dangeo slotu, ustawia skrot klawiszowy oraz ustawia prametr checable i checked
Argumenty:
yourself(self/QMainWindow/QWidget): rodzic akcji
actionName(str): nazwa akacji
addToMenu(QMenu): menu do ktorego ma zostac dodana akcja
connectTriggered: slot do ktorego nalezy podlaczyc akcje (dla argumenut= "" nie podlacza)
shortcut(str): skrot klawiszowy (dla argumentu = "" nie podlacza)
checkable(bool): czy akcja ma byc jak checkbox
checked(bool): czy akcja ma wartosc true czy false

Zwraca
action(QAction): akcja ktora zostala ustworzona
'''
def createAction(yourself, actionName, addToMenu, connectTriggered, shortcut, checkable = False, checked = False):
    action = QAction(actionName,yourself)
    if(connectTriggered != ""): action.triggered.connect(connectTriggered)
    if(shortcut != ""): action.setShortcut(shortcut)
    action.setCheckable(checkable)
    action.setChecked(checked)
    addToMenu.addAction(action)
    return action

'''Glowne okno czyli klasa, ktora laczy ze soba wszystkie klasy
Hierarchia klasy (pogladowo):
        MainWindow:
                Ustawienia zmiennych:
                -> self.manualParmsWindow (DynamicDialog) używa słownika self.manualParms
                -> self.nestingParmsWindow (DynamicDialog) używa słownika self.nestingParms
                -> self.debugWindow (DynamicDialog) używa słownika self.debugParms
                
                Reprezentacja figur
                -> self.figures (MyFigure) 
                
                Widok:
                -> self.gl (PreviewGL) uzywa self.figures poprzez funkcje self.gl.setDrawData(), ustawienia kolizji odbywaja sie przez self.gl.setNestingData()
                
                Nesting
                -> self.nestingClass uzywa self.figures oraz self.nestingParms
'''
class MainWindow(QMainWindow):
# ---- INICJALIZACJA KLASY ----#
    '''inicjalizacja klasy'''
    def __init__(self): 
        QMainWindow.__init__(self)
        self.variables() #inicjalizacja zmiennych
        self.initUi() #inicjalizacja gui
        self.show()
        
    '''zmienne klasy'''
    def variables(self):
    #wczytywanie pliku
        self.fname = "" #sciezka do pliku
        self.figures = [] #wczytane figury
     
    #losowanie figury
        self.randomFigureParms = {}
        self.randomFigureParms["ctrX"] = 1.0
        self.randomFigureParms["ctrY"] = 1.0
        self.randomFigureParms["aveRadius"] = 25
        self.randomFigureParms["irregularity"] = 0.5
        self.randomFigureParms["spikeyness"] = 0.5
        self.randomFigureParms["numVerts"] = 7 
        
    #posuw reczny :p
        self.manualParms = {}
        self.manualParms["moveBy"] = 1
        self.manualParms["rotateBy"] = 5
        self.manualParms["movingFigure"] = 0
        self.manualParms["zoom"] = 5
        self.manualParms["moveView"] = 5
        
    #zmienne do nestingu
        self.nestingParms = {}
        self.nestingParms["nestingRotateValue"] = 180.0 #rotacja
        self.nestingParms["nestingSpace"] = 1.0 #przykladanie/promien
        self.nestingParms["nestingMoveAlgorithm"] = True #uzyc algorytmu Move?
        self.nestingParms["nestingCircleAlgorithm"] = False #uzyc algorytmu Cicrcle?
        self.nestingParms["useArea"] = False #do najlepszego dopasowania uzyj prostego algorytmy pola
        self.nestingParms["useCentroid"] = True #do najlepszego dopasowania uzyj centroida
        self.nestingParms["epsCentroid"] = 0.01 #gdy do dopasowania uzywany jest algorytm pola i centrooida to jest to wspolczynnik zaniedbywalnosci centroida wzgledem pola
        self.nestingParms["circleAngle"] = 10 #kat o jaki porusza sie figura w algorytmie cicrcle
        #wspolrzedne stolu
        self.nestingParms["tablebeginx"] = 0.0
        self.nestingParms["tablebeginy"] = 0.0
        self.nestingParms["tableendx"] = 100.0
        self.nestingParms["tableendy"] = 100.0
        self.nestingParms["table"]= [Point(self.nestingParms["tablebeginx"], self.nestingParms["tablebeginy"],0.0),\
                      Point(self.nestingParms["tableendx"] , self.nestingParms["tableendy"],0.0)]
        
        self.nestingClass = Nesting() #klasa nestingowa
        
    #DEBUGOWANIE
        self.debugParms = {}
        #run.py
        self.debugParms["debugLoadFile"] = False
        #self.debugParms["debugNestingMovinFigure"] = False
        #self.debugParms["debugLoadFile"] = False
        #Nesting.py
        self.debugParms["debugNestingData"] = False
        self.debugParms["debugMoveToPoint"] = False
        self.debugParms["debugMoveToPoints"] = False
        self.debugParms["debugNestingStart"] = False
        #MyFigure.py
        self.debugParms["returnFirst"] = True
        self.debugParms["debugCentroid"] = False
        self.debugParms["debugTriangulation"] = False
        self.debugParms["debugPointInsideTriangle"] = False
        self.debugParms["debugMinMax"] = False
        self.debugParms["debugCalcLinearParm"] = False
        self.debugParms["debugCalcLinearParms"] = False
        self.debugParms["debugLineCutting"] = False
        self.debugParms["debugColisionTriangles"] = False
        self.debugParms["debugFind"] = False
        
        self.setDebugValues()
        
    '''stworzenie gui'''
    def initUi(self):
    #opengl
        self.gl = PreviewGL(self)
        self.setCentralWidget(self.gl)
        self.gl.show()
    
    #pasek menu
        fileMenu = self.menuBar().addMenu('Plik')
        openAction = createAction(self,'Otworz', fileMenu, self.actionLoad,  "Ctrl+O") #akcja otworz
        saveAction = createAction(self,'Zapisz', fileMenu, self.actionSave,  "Ctrl+S") #akcja otworz
        #saveConfigurationAction = createAction(self,'Zapisz konfiguracje', fileMenu, self.saveAllParms,  "") #akcja otworz
        closeAction = createAction(self,'Zamknij', fileMenu, self.close, "") #akcja zamknij
       
     #widok
        viewMenu = self.menuBar().addMenu('Widok')
        zoomInAction = createAction(self, 'Przybliz ', viewMenu, self.actionZoomIn, "Ctrl++") #rozpoczecie nestingu
        zoomOutAction = createAction(self, 'Oddal ', viewMenu, self.actionZoomOut, "Ctrl+-") #rozpoczecie nestingu
        moveViewLeftAction = createAction(self, 'Przesun w lewo ', viewMenu, self.actionViewLeft, "Z") #rozpoczecie nestingu 
        moveViewRightAction = createAction(self, 'Przesun w prawo ', viewMenu, self.actionViewRght, "X") #rozpoczecie nestingu 
        moveViewUpAction = createAction(self, 'Przesun w gore ', viewMenu, self.actionViewUp, "A") #rozpoczecie nestingu 
        moveViewDownAction = createAction(self, 'Przesun w dol ', viewMenu, self.actionViewDown, "S") #rozpoczecie nestingu 
        
    #posuw reczny
        #ustawienia
        manualMenu = self.menuBar().addMenu('Posuw reczny')
        self.manualParmsWindow = DynamicDialog([\
                                                        ["label", "Poruszaj o","moveBy",self.manualParms["moveBy"]],\
                                                        ["label", "Rotuj o", "rotateBy", self.manualParms["rotateBy"]],\
                                                        ["label", "Poruszana figura", "movingFigure", self.manualParms["movingFigure"]],\
                                                        ["label", "Oddalanie/Przyblizanie widoku o", "zoom", self.manualParms["zoom"]],\
                                                        ["label", "Przesuwanie widoku o","moveView", self.manualParms["moveView"]]\
                                               ],\
                                               "Posuw reczny")
        self.manualParmsWindow.s_ValueChanged.connect(self.signalFromManualOptions)
        manualControlAction = createAction(self, 'Sterowanie', manualMenu, self.controlInformationAction, "") 
        manualOptionsAction = createAction(self, 'Ustawienia', manualMenu, self.actionManualOptions, "") 
        
        
    #pasek Nesting
        fileNesting = self.menuBar().addMenu('Nesting')
        #nestingStartAction = createAction(self, 'Start', fileNesting, self.nestingStart, "F1") 
        
        #Okno parametrow nestingu
        self.nestingParmsWindow = DynamicDialog([\
                                                        ["none","Parametry"],\
                                                        ["label","Odstep od punktu","nestingSpace",self.nestingParms["nestingSpace"]],\
                                                        ["label","Rotacja","nestingRotateValue",self.nestingParms["nestingRotateValue"]],\
                                                        
                                                        ["none","Stol"],\
                                                        ["label","Lewy brzeg X:","tablebeginx",self.nestingParms["tablebeginx"]],\
                                                        ["label","Lewy brzeg Y:","tablebeginy",self.nestingParms["tablebeginy"]],\
                                                        ["label","Prawy brzeg X:","tableendx",self.nestingParms["tableendx"]],\
                                                        ["label","Prawy brzeg Y:","tableendy",self.nestingParms["tableendy"]],\
                                                        
                                                        ["none","Przysuwania do punktu"],\
                                                        ["checkbox","8 punktow","nestingMoveAlgorithm",self.nestingParms["nestingMoveAlgorithm"]],\
                                                        ["checkbox","Krazenie do okola","nestingCircleAlgorithm",self.nestingParms["nestingCircleAlgorithm"]],\
                                                        ["label","Kat o jaki ma krazyc","circleAngle",self.nestingParms["circleAngle"]],\
                                                        
                                                        ["none","Najlepszy obszar"],\
                                                        ["checkbox","Dopasowuj przy uzyciu pola","useArea",self.nestingParms["useArea"]],\
                                                        ["checkbox","Dopasowuj przy uzyciu centroida","useCentroid",self.nestingParms["useCentroid"]],\
                                                        ["label","Zaniedbywalnosc centroida \n(oba dopasowanie zaznaczone)","epsCentroid",self.nestingParms["epsCentroid"] ]\
                                                ],\
                                                "Nesting start"
                                                )
        self.nestingParmsWindow.s_ValueChanged.connect(self.signalFromNestingOptions)
        nestingStartAction = createAction(self, 'Nesting start', fileNesting, self.actionNestingOptions, "") #rozpoczecie nestingu
        
        #Okno losowania figury
        self.randomFigureWindow = DynamicDialog([\
                                                ["none","Parametry losowanej figury"],\
                                                ["label","Srodek figury wspolrzedna X","ctrX",self.randomFigureParms["ctrX"]],\
                                                ["label","Srodek figury wspolrzedna Y","ctrY",self.randomFigureParms["ctrY"]],\
                                                ["label","Sredni promien figury","aveRadius",self.randomFigureParms["aveRadius"]],\
                                                ["label","irregularity (0-1)","irregularity",self.randomFigureParms["irregularity"]],\
                                                ["label","spikeyness (0-1)","spikeyness",self.randomFigureParms["spikeyness"]],\
                                                ["label","ilosc wierzcholkow","numVerts",self.randomFigureParms["numVerts"]]
                                        ],\
                                        "Losowanie figury"
                                        )
        self.randomFigureWindow.s_ValueChanged.connect(self.signalAddFigure)
        randomFigureAction = createAction(self, 'Losowanie Figury', fileNesting, self.actionRandomFigureWindow, "") #
        
        deleteLastFigureAction = createAction(self, 'Usun ostatnia figure', fileNesting, self.actiondeleteLastFigure, "Delete") #
        
    #pasek Dodatkowe 
        fileExtra = self.menuBar().addMenu('Dodatkowe')
        self.debugWindow = DynamicDialog([\
                                                ["none","run.py"],\
                                                ["checkbox","debugLoadFile","debugLoadFile",self.debugParms["debugLoadFile"] ],\
                                                #["checkbox","debugNestingMovinFigure","debugNestingMovinFigure",self.debugParms["debugNestingMovinFigure"]],\
                                                #["checkbox","debugNesting","debugLoadFile",self.debugParms["debugLoadFile"]],\
                                                
                                                ["none","Nesting.py"],\
                                                ["checkbox","debugNestingData","debugNestingData",self.debugParms["debugNestingData"] ],\
                                                ["checkbox","debugMoveToPoint","debugMoveToPoint",self.debugParms["debugMoveToPoint"]],\
                                                ["checkbox","debugMoveToPoints","debugMoveToPoints",self.debugParms["debugMoveToPoints"]],\
                                                ["checkbox","debugNestingStart","debugNestingStart",self.debugParms["debugNestingStart"] ],\
                                                
                                                ["none","MyFigure.py"],\
                                                ["checkbox","returnFirst","returnFirst",self.debugParms["returnFirst"] ],\
                                                ["checkbox","debugCentroid","debugCentroid",self.debugParms["debugCentroid"]],\
                                                ["checkbox","debugTriangulation","debugTriangulation",self.debugParms["debugTriangulation"] ],\
                                                ["checkbox","debugPointInsideTriangle","debugPointInsideTriangle",self.debugParms["debugPointInsideTriangle"] ],\
                                                ["checkbox","debugMinMax","debugMinMax",self.debugParms["debugMinMax"] ],\
                                                ["checkbox","debugCalcLinearParm","debugCalcLinearParm",self.debugParms["debugCalcLinearParm"]],\
                                                ["checkbox","debugCalcLinearParms","debugCalcLinearParms",self.debugParms["debugCalcLinearParms"]],\
                                                ["checkbox","debugLineCutting","debugLineCutting",self.debugParms["debugLineCutting"] ],\
                                                ["checkbox","debugColisionTriangles","debugColisionTriangles",self.debugParms["debugColisionTriangles"] ],\
                                                ["checkbox","debugFind","debugFind",self.debugParms["debugFind"] ]\
                                        ],\
                                        "Debugowanie"
                                        )
        debugAction = createAction(self, 'Debugowanie', fileExtra, self.actionDebug, "") #rozpoczecie nestingu
        self.debugWindow.s_ValueChanged.connect(self.signalFromDebug)
       
        #pasek render
        fileRender = fileExtra.addMenu('Render')
        traingulationAction = createAction(self, 'Triangulation', fileRender, self.gl.setDrawingFlagTriangle, "T", True, True) #traingulation
        centroidAction = createAction(self, 'Centroid', fileRender, self.gl.setDrawingFlagCentroid, "C", True, False) #Centroid
        megaCentroidAction = createAction(self, 'Centroid Wszystich Figur', fileRender, self.gl.setDrawingFlagMegaCentroid, "V", True, True) #Centroid
        polygonAction = createAction(self, 'Polygon', fileRender, self.gl.setDrawingFlagPolygon, "P", True, True) #Polygon
        
    #ustawienie statusbara (zeby byl widoczny od poczatku)
        self.statusBarMessage("")
       
# ---- ZAPIS/ODCZYT KONFIGURACJI----#
    '''sprawdza czy istnieje plik z parametrami, jesli tak to go wczytuje'''
    def checkIfOptionsAreSaved(self): #TODO
        fname = os.path.split(os.path.abspath(__file__))[0]
        fname += "/runOptions"
        print "znaleziono plik konfiguracyjny"
        
        if( os.path.isfile(fname) ):
            f = open(fname ,"r")
            d = json.load(f)
            f.close()
                
            if "manualParms" in d:
                self.manualParms = d["manualParms"] 
                print "mm"
            
            if "debugParms" in d:
                self.debugParms = d["debugParms"] 
                print "deb"
                
            if "nestingParms" in d:
                self.nestingParms = d["nestingParms"] 
                print "nest"
        
    '''zapisuje wszystkie parametru do pliku'''
    def saveAllParms(self): #TODO
    #odczytanie sciezki
        fname = os.path.split(os.path.abspath(__file__))[0]
        fname += "/runOptions"
        
    #przygotwanie do zapisu
        toSave = {}
        toSave["manualParms"] = self.manualParms
        toSave["debugParms"] = self.debugParms
        toSave["nestingParms"] = self.nestingParms
        
    #zapis wlasciwy
        with open(fname, "w'") as outfile:
                json.dump(toSave, outfile)
        
# ---- LOSOWANIE FIGURY----#
    '''losowanie nowej figury na podstawie parametrow z okna actionRandomFigureWindow '''
    def signalAddFigure(self):
        dic = {}
        if(self.randomFigureWindow.readValues(dic) == True):
                self.randomFigureParms = dic
                
                if(self.randomFigureParms["irregularity"]) > 1:
                        self.randomFigureParms["irregularity"] = 1
                elif(self.randomFigureParms["irregularity"]) < 0:
                        self.randomFigureParms["irregularity"] = 0
                
                if(self.randomFigureParms["spikeyness"]) > 1:
                        self.randomFigureParms["spikeyness"] = 1
                elif(self.randomFigureParms["spikeyness"]) < 0:
                        self.randomFigureParms["spikeyness"] = 0
                
                self.randomFigureParms["numVerts"] = int(self.randomFigureParms["numVerts"])
                
                if(self.randomFigureParms["numVerts"] >= 3):
                        start = time.time()
                        randomFigure = PolygonGenerator(self.randomFigureParms["ctrX"],\
                                                        self.randomFigureParms["ctrY"],\
                                                        self.randomFigureParms["aveRadius"],\
                                                        self.randomFigureParms["irregularity"],\
                                                        self.randomFigureParms["spikeyness"],\
                                                        self.randomFigureParms["numVerts"])
                        self.figures.append(randomFigure.getFigure())
                        self.gl.setDrawData(self.figures)
                        self.gl.update()
                        end = time.time()
                        print "Wyloswanie figury zajelo:", (start - end)
                else:
                    print "zbyt mala liczba wierzcholkow"
    
    '''otwiera okno sluzace do generowania wielokatow'''
    def actionRandomFigureWindow(self): 
        self.randomFigureWindow.show()
       
    '''usuniecie ostatniej figury'''
    def actiondeleteLastFigure(self): 
        if len(self.figures) > 0:
            del self.figures[0]
            self.gl.setDrawData(self.figures)
            self.gl.update()
            
# ---- OPERACJE NA WIDOKU----#

    '''Przyblizenie widoku'''
    def actionZoomIn(self):
        self.gl.changeZoomView( -self.manualParms["zoom"] )
    
    '''Oddalenie widoku'''
    def actionZoomOut(self):
        self.gl.changeZoomView( self.manualParms["zoom"] )

    '''Przesuniecie widoku w lewo'''
    def actionViewLeft(self):
        self.gl.moveView( -self.manualParms["moveView"], 0.0 )
    
    '''Przesuniecie widoku w prawo'''
    def actionViewRght(self):
        self.gl.moveView( self.manualParms["moveView"], 0.0 )

    '''Przesuniecie widoku w gore'''
    def actionViewUp(self):
        self.gl.moveView( 0.0 , self.manualParms["moveView"] )

    '''Przesuniecie widoku w dol'''
    def actionViewDown(self):
        self.gl.moveView( 0.0 , -self.manualParms["moveView"] )
        
# ---- ZAPISYWANIE PLIKU ----#
    '''Zapisywanie pliku'''
    def actionSave(self):
        fname = QFileDialog.getSaveFileName(self, "Zapisywanie pliku", "", "*.json")
        if(fname != ""):
            #poprawienie nazwy pliku (a wlasciewie jego koncowki)
                test = str(fname).split(".")
                if( test[-1] != "json" ):
                        fname += ".json"
            
            #przygotowanie zawrtosci pliku
                toSave = {}
                toSaveFigure = []
                for figure in self.figures:
                        toSavePoint = []
                        for point in figure.v:
                            toSavePoint.append([point.x,point.y])
                        toSaveFigure.append(toSavePoint)
                toSave["figures"] = toSaveFigure
                
            #zapis wlasciwy
                with open(fname, "w'") as outfile:
                        json.dump(toSave, outfile)
               
# ---- WCZYTYWANIE PLIKU ----#
    '''wczytywanie sciezki do pliku'''
    def actionLoad(self):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.AnyFile)
        dlg.setFilter("JavaScript Object Notation (*.json);;Data Exchange Format(*.dxf)")

        filename = QStringList()
        if dlg.exec_():
            filename = dlg.selectedFiles()
        
        if(filename.count() != 0):
            filename = filename.takeFirst() #uwaga na polskie znaki
            
            if(os.path.isfile(filename)):
                self.fname = filename
                fnameType = str(self.fname).split(".")[-1]
                
                self.statusBarMessage("wczytywanie pliku")
                
                if(fnameType == "json"):
                    
                    self.loadJson()
                else:
                    self.loadDxf()
        else:
            self.statusBarMessage("bledna sciezka do pliku")
        
    '''wczytywanie pliku wymyslonego do testow'''
    def loadJson(self):
        if(self.fname != ""):
            try:
                X = 0
                Y = 1
                
                figures = []
                if (self.debugParms["debugLoadFile"]): print "self.fname", self.fname
                f = open(self.fname ,"r")
                d = json.load(f)
                f.close()
                d = d["figures"]
                
                
                area = 0.0
                for figure in d:
                    points = []
                    
                    for point in figure:
                        points.append(Point(float(point[X]),float(point[Y]),0.0))
                        if (self.debugParms["debugLoadFile"]):  print points[-1]
                    
                    if len(points) >= 3:
                        figures.append(MyFigure(points,True, self.debugParms))
                        area += figures[-1].areaSum
                    elif (self.debugParms["debugLoadFile"]):  print "figura, ma mniej niz 3 pkt dlatego zostanie zignorowana"
            
                if (self.debugParms["debugLoadFile"]):  
                    print "plik wczytany"
                print "Pole wszystkich wczytanych figur wynosi:", area
                
                
                self.figures = figures
                #self.drwaBorderFigure()
                self.gl.setNestingData([])
                self.gl.setDrawData(self.figures)
                
                self.statusBarMessage("Liczba figur:"+ str(len(self.figures)))
            except:
                print "nie udalo sie wczytac pliku"
                self.statusBarMessage("nie udalo sie wczytac pliku")
                gl.clear();

    '''wczytywanie formatu DXF''' #TODO
    def loadDxf(self):
        print "Tutaj powinno byc funkcja wczytujaca dxf, ale jej nie ma :p"
        
# ---- DEBUGOWANIE----#
    '''Otworzenie okna parametrow debugowania'''
    def actionDebug(self):
        self.debugWindow.show()
    
    '''Obsluga syngalu informujace o zmianie parametrow debugowania'''
    def signalFromDebug(self):
        dic = {}
        if(self.debugWindow.readValues(dic) == True):
            self.debugParms = dic
            print "Parametry debugowania ustawione"
            #print "Parametry do debugowania", self.debugParms
            self.setDebugValues()
        else:
            print "Bledne parametry"
    
    '''Przypisanie otrzymanych wartosci debugowania do pozostalych klas'''
    def setDebugValues(self):
        self.nestingClass.setDebugValues(self.debugParms)
        
        for figure in self.figures:
            figure.setDebugValues(self.debugParms)
        
# ---- NESTING----#
    '''Otworzenie okna parametrow nestingu'''
    def actionNestingOptions(self): 
        self.nestingParmsWindow.setValues(self.nestingParms)
        self.nestingParmsWindow.show()
            
    '''Obsluga syngalu informujace o zmianie parametrow nestingu'''
    def signalFromNestingOptions(self): 
        dic = {}
        if(self.nestingParmsWindow.readValues(dic) == True):
            for key in dic.keys():
                #print "key:", key
                if key in self.nestingParms.keys():
                    self.nestingParms[key] = dic[key]
            self.nestingClass.setNestingValues(dic)
            print "Parametry nestingu ustawione"
            self.table = [Point(self.nestingParms["tablebeginx"], self.nestingParms["tablebeginy"],0.0),\
                      Point(self.nestingParms["tableendx"] , self.nestingParms["tableendy"],0.0)]
            self.gl.setTableData(self.table)
            self.nestingStart()
            #print "Parametry nestingu po zmianie \n",self.nestingParms
        else:
            print "Bledne parametry"

    '''Rozpoczecie nestingu'''
    def nestingStart(self):
        if len(self.figures) > 1:
                start = time.time()
                self.nestingClass.setNestingData( self.figures ) #ustawienie zmiennych do nestingu
                self.nestingClass.nestingStart()
               
                self.figures = copy.deepcopy(self.nestingClass.anf)
                self.figures.extend( copy.deepcopy(self.nestingClass.cnf) )
                
                self.gl.setDrawData(self.figures)
                self.gl.setNestingData([])
                
                end = time.time()
                self.statusBarMessage("Ulozenie zajelo:" + str((end-start)))
                print "Ulozenie zajelo:", (end-start)
            
# ---- POSUW RECZNY  ----#
    '''Testowanie obramowania'''
    def drwaBorderFigure(self): #TODO
        border = 2
        figure = copy.deepcopy(self.figures[-1])
        figure.calcLinearParms()
        cx = figure.cx
        cy = figure.cy
        print "sdfsdfsd", cx, cy
        for i, point in enumerate(figure.v):
            figure.v[i].x *= 1.1
            figure.v[i].y *= 1.1
            #print figure.v[i].x, figure.v[i].y 
            #newx = point.x  - cx
            
            #if(abs(newx) < 0.01):
                #newx = 0.0
            #elif(newx > 0):
                #newx = 0.0 + border
            #else:
                #newx = 0.0 - border
            #figure.v[i].x += newx
            
            #newy = point.y  - cy
            #if(abs(newy) < 0.01):
                #newy = 0.0
            #elif(newy > 0):
                #newy = 0.0 + border
            #else:
                #newy = 0.0 - border
            #figure.v[i].y += newy
            #print figure.v[i].x, figure.v[i].y 
            
            
        figure.calcLinearParms()
        figure.calcCentroid()
        figure.findMinMax()
        
        self.figures.append(figure)
                
    '''Informacja na temat sterowania posuwem recznym'''
    def controlInformationAction(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText("STRZALKI - przesuwanie figury\n E/R - obrot figury")
        msg.setWindowTitle("Sterowanie posuwem recznyms")
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()
    
    '''Obsluga syngalu informujace o zmianie parametrow posuwu recznego'''
    def signalFromManualOptions(self): 
        dic = {}
        if(self.manualParmsWindow.readValues(dic) == True):
            for key in dic.keys():
                if key in self.manualParms.keys():
                    self.manualParms[key] = dic[key]
            self.manualParms["movingFigure"] = int(self.manualParms["movingFigure"])
            print "Parametry posuwu recznego ustawione"
            #print "Parametry nestingu po zmianie \n",self.nestingParms
        else:
            print "Bledne parametry"

    '''Otworzenie okna parametrow posuwu recznego'''
    def actionManualOptions(self): 
        self.nestingParmsWindow.setValues(self.manualParms)
        self.manualParmsWindow.show()

    '''ustawienie informacji na status barze'''
    def statusBarMessage(self,text): #funkcja pomocniczna do colisionTest
        self.statusBar().showMessage(text)
    
    '''sprawdzenie kolizji dla posuwu recznego'''
    def colisionTest(self): #test kolizji (dla gui) dla 2 figur
        if(len(self.figures)>1):
            test = False
            p = []
            before = self.figures[self.manualParms["movingFigure"]].debugLineCutting
            self.figures[self.manualParms["movingFigure"]].debugLineCutting = True
            for i in range(0, len(self.figures)):
                if(i != self.manualParms["movingFigure"]):
                        ttest, tp = self.figures[self.manualParms["movingFigure"]].colisionTriangles(self.figures[i])
                        test += ttest
                        if(ttest):
                                p.append(MyFigure(tp,False))
                        print "i:",i,"test:",ttest
                        for a in tp: print a
                
            self.figures[self.manualParms["movingFigure"]].debugLineCutting = before
            
            Min = self.figures[0].Min
            Max = self.figures[0].Max
            for i in range(1, len(self.figures)):
                tmp = self.figures[i]
                if(Min.x > tmp.Min.x):
                    Min.x = tmp.Min.x
                if(Min.y > tmp.Min.y):
                    Min.y = tmp.Min.y
                if(Max.x < tmp.Max.x):
                    Max.x = tmp.Max.x
                if(Max.y < tmp.Max.y):
                    Max.y = tmp.Max.y
                    
            area = abs(Min.x - Max.x)*abs(Min.y - Max.y)
            print "Pole wynosi:", area
                
                
            
            if(test):
                print "kolizja"
                self.statusBarMessage("Kolizja")
                self.gl.setNestingData(p)
                return True
            else:
                print "brak kolizji"
                self.gl.setNestingData([])
                self.statusBarMessage("Liczba figur:"+ str(len(self.figures)))
                return False
        
# ---- EVENTY ----#
    '''obsluga klawiatury'''
    def keyPressEvent(self, event):
        if(len(self.figures) != 0):
            key = event.key()
            if key == QtCore.Qt.Key_Left:
                self.figures[self.manualParms["movingFigure"]].move(-self.manualParms["moveBy"],0)
                self.figures[self.manualParms["movingFigure"]].calcLinearParms()
                self.colisionTest()
                self.gl.setDrawData(self.figures)
                                    
            if key == QtCore.Qt.Key_Right:
                self.figures[self.manualParms["movingFigure"]].move(self.manualParms["moveBy"],0)
                self.figures[self.manualParms["movingFigure"]].calcLinearParms()
                self.colisionTest()
                self.gl.setDrawData(self.figures)
                
            if key == QtCore.Qt.Key_Down:
                self.figures[self.manualParms["movingFigure"]].move(0,-self.manualParms["moveBy"])
                self.figures[self.manualParms["movingFigure"]].calcLinearParms()
                self.colisionTest()
                self.gl.setDrawData(self.figures)
                
            if key == QtCore.Qt.Key_Up:
                self.figures[self.manualParms["movingFigure"]].move(0,self.manualParms["moveBy"])
                self.figures[self.manualParms["movingFigure"]].calcLinearParms()
                self.colisionTest()
                self.gl.setDrawData(self.figures)
                
            if key == QtCore.Qt.Key_R:
                x = self.figures[self.manualParms["movingFigure"]].Min.x
                y = self.figures[self.manualParms["movingFigure"]].Min.y
                self.figures[self.manualParms["movingFigure"]].rotate(self.manualParms["rotateBy"])
                self.figures[self.manualParms["movingFigure"]].calcLinearParms()
                self.colisionTest()
                self.gl.setDrawData(self.figures)
                
            if key == QtCore.Qt.Key_E:
                x = self.figures[self.manualParms["movingFigure"]].Min.x
                y = self.figures[self.manualParms["movingFigure"]].Min.y
                self.figures[self.manualParms["movingFigure"]].rotate(-self.manualParms["rotateBy"])
                self.figures[self.manualParms["movingFigure"]].calcLinearParms()
                self.colisionTest()
                self.gl.setDrawData(self.figures)
                
    '''zadabnie o to by zamykaly sie wszystkie okienka'''
    def closeEvent(self, evnt):
        self.nestingParmsWindow.close()
        self.debugWindow.close()
        self.manualParmsWindow.close()
        self.randomFigureWindow.close()
        self.close()
        
# ---- PYTHON RUN ----#
if __name__ == '__main__':
    app = QApplication(sys.argv)
    root = MainWindow()
    ret = app.exec_()
    sys.exit(ret)
